from flask import Blueprint, session, request, jsonify

from common.function_fetch import site_mail_add
from common.function_fetch import user_disabled
from common.function_fetch import check_user_status
from module.site_mail import *

site_mail = Blueprint('site_mail', __name__)


@site_mail.route("/site/mail/query", methods=['GET'])
def site_mail_query():
    u_id = session.get('user_id')
    check_user_status(u_id)
    
    page_num = request.args.get("page_num")
    if page_num is None:
        page_num = 1
    else:
        page_num = int(page_num)
    page_size = request.args.get("page_size")
    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)
    
    resp = []
    mails = SiteMail.get_by_user_id(u_id, page_num, page_size)
    for mail in mails.items:
        resp.append(mail.to_json())

    return jsonify(resp)


@site_mail.route("/site/mail/add", methods=['POST'])
def site_mail_add_app():
    u_id = session.get('user_id')
    check_user_status(u_id)
    
    headline = request.json.get("headline")
    if not headline:
        return {"info": "标题为空" ,"code": 501}
    
    mail_content = request.json.get("mail_content")
    if not mail_content:
        return {"info": "内容为空" ,"code": 501}
    
    site_mail_add(u_id, headline, mail_content)
    return  {"info": "success" ,"code": 0}




@site_mail.route("/site/mail/delete", methods=['POST'])
def site_mail_delete():
    u_id = session.get('user_id')
    check_user_status(u_id)
    
    mail_id = request.json.get("mail_id")
    if mail_id is None:
        return {"info": "mail_id为空" ,"code": 501}
    
    SiteMail.delete_by_id(mail_id)
    return  {"info": "success" ,"code": 0}


@site_mail.route("/site/mail/<int:mail_id>", methods=['GET'])
def site_mail_get(mail_id):
    u_id = session.get('user_id')
    check_user_status(u_id)
    
    mail = SiteMail.get_by_id(mail_id)
    if not mail:
        return {"info": "信件不存在" ,"code": 501}
    SiteMail.status_update(mail_id, 1)
    return  {"info": "success" ,"code": 0}


@site_mail.route("/site/mail/readall", methods=['POST'])
def site_mail_readall():
    u_id = session.get('user_id')
    check_user_status(u_id)
    
    SiteMail.status_update_user_all(u_id, 1)
    return  {"info": "success" ,"code": 0}