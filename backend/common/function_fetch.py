
from flask import session

from module.site_mail import *
from module.user import *


def user_disabled(user_id):
    user = User().get_by_id(user_id)
    if user and user.black == 1:
        session['isLogin'] = 'false'
        session['user_id'] = 0
        session['user_name'] = ''
        session['type'] = ''
        return True
    else:
        return False

def check_user_status(u_id):
    if not u_id:
        return {"info": "not login", "code": 1}  # 未等录
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}

def site_mail_add(user_id, headline, mail_content):
    mail = SiteMail()
    mail.user_id = user_id
    mail.headline = headline
    mail.mail_content = mail_content
    mail.add()
