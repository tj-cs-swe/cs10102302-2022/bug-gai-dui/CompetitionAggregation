package com.ooo.stu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Luogu {

    public static List<Game> findGame() {
        List<Game> games = new ArrayList<>();
        try {
            Connection.Response document = Jsoup.connect("https://www.luogu.com.cn/contest/list?page=1&_contentOnly=1")
                    .header("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .execute();
            String body = document.body();

            body = StringEscapeUtils.unescapeJava(body);

            System.out.println(body);

            JSONArray datas = JSON.parseObject(body).getJSONObject("currentData").getJSONObject("contests").getJSONArray("result");
            for(int i=0;i<datas.size();i++) {
                JSONObject obj = datas.getJSONObject(i);
                String name = obj.getString("name");
                Long startTimeS = obj.getLong("startTime");
                Long endTimeS = obj.getLong("endTime");
                Date startTime = new Date(startTimeS * 1000);
                Date endTime = new Date(endTimeS  * 1000);

                String duration = TimeUtils.getDatePoor(startTime,endTime);

                String id = obj.getString("id");
                String href = "https://www.luogu.com.cn/contest/" + id;
                String group = obj.getString("ruleType");
                String difficulty = "1";
                String oid = "luogu-" + id;
                Game game = new Game();
                game.setTitle(name);
                game.setStartDate(startTime);
                game.setEndDate(endTime);
                game.setDuration(duration);
                game.setLink(href);
                game.setGroup(group);
                game.setOid(oid);
                game.setDifficulty(difficulty);
                game.setPlatform("洛谷");
                games.add(game);
            }
            for(Game g: games) {
                System.out.println(g);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return games;
    }

}
