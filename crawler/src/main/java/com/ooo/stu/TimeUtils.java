package com.ooo.stu;

import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

public class TimeUtils {

    public static String getDatePoor(Date stDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = nowDate.getTime() - stDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        StringBuilder sb = new StringBuilder();
        if(day > 0) {
            sb.append(day).append("day ");
        }
        if(hour > 0) {
            if (hour <= 9){
                sb.append("0").append(hour).append(":");
            } else
                sb.append(hour).append(":");
        } else if (hour == 0){
            sb.append("00").append("");
        }
        if(min > 0) {
            sb.append(min).append("");
        } else if (min == 0){
            sb.append("00").append("");
        }
        return  sb.toString() ;
    }

    public static String format(Date time){
        return DateFormatUtils.format(time,"yyyy-MM-dd HH:mm:ss");
    }
}
